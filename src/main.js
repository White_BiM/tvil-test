/* eslint-disable no-console */
import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
Vue.use(Vuex, VueRouter)

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App),
    router,
    store,
    created() {
        this.$store.dispatch('fetchChosenHotel')
        this.$store.dispatch('fetchHotels')
    }
}).$mount('#app')
