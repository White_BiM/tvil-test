import Vue from 'vue'
import Router from 'vue-router'
import HotelsList from '@/components/HotelsList'
import Reservation from '@/components/Reservation'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', redirect: '/HotelsList' },
        {
            path: '/HotelsList',
            props: true,
            name: 'HotelsList',
            component: HotelsList
        },
        {
            path: '/Reservation/:id',
            props: true,
            name: 'Reservation',
            component: Reservation
        },
    ],
    scrollBehavior(){
        return{x:0,y:0}
    }
})