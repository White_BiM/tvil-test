
import hotelsList from '../hotels'

Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
}

export default {
    state: {
        hotels: [],
        chosenHotelId: null
    },
    mutations: {
        setHotels(state, payload){
            state.hotels = payload
            let hotels = state.hotels
            localStorage.setObj('hotelsList', hotels)
        },
        setChosenHotel(state, payload){
            state.chosenHotelId = payload
        },
        chooseHotel(state, payload){
            state.chosenHotelId = payload
            localStorage.setObj('chosenHotelId', payload)
        },
    },

    actions: {
        async fetchHotels({commit}){
            try{
                commit ('setHotels', hotelsList)
            } catch(error){
                throw error
            }
        },
        async fetchChosenHotel({commit}){
            try{
                let chosenHotel = await localStorage.getObj('chosenHotelId')
                if (chosenHotel) {
                    commit ('setChosenHotel', chosenHotel)
                }
            } catch(error){
                throw error
            }
        },
        async chooseHotel({commit}, payload){
            try{
               await commit ('chooseHotel', payload)
            } catch(error){
                throw error
            }
        },
    },

    getters:  {
        hotels (state) {
            return state.hotels
        },
        hotelById (state) {
            return hotelId => {
                return state.hotels.find(hotel => hotel.id === hotelId)
            }
        },
        chosenHotelId(state) {
            return state.chosenHotelId
        },
    }
}