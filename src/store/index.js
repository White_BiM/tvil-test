import Vue from "vue"
import Vuex from 'vuex'
import hotels from './hotels'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        hotels
    }

})
